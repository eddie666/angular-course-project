import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingBookComponent } from './shopping-book.component';

describe('ShoppingBookComponent', () => {
  let component: ShoppingBookComponent;
  let fixture: ComponentFixture<ShoppingBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoppingBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
