import { Subject } from 'rxjs'
import { Ingredient } from '../shared/ingredient.model';

export class ShoppingListService {

  // добавим новый eventemitter для добавления нового игредиента в массив ингредиентов с джинериком Ingredient[];
  ingredientChanged = new Subject<Ingredient[]>();
  // добавим новый Subject для того чтобы слушать shopping list edit component
  startedEditing = new Subject<number>();

  // добавляем проперти с массивом ингридиентов
  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatos', 10)
  ];
  // добавляем метод getIngredients по аналогии с getRecipes
  getIngredients() {
    return this.ingredients.slice();
  }

  // добавим метод, который будет возвращать нам ингридиент по index для дальнейшего редактирования
  getIngredient(index: number) {
    return this.ingredients[index];
  }


  // добавляем метод addIngredient для добавления ингридентов в массив ingredients параметры ingredient с типом Ingredient
  addIngredient(ingredient: Ingredient) {
    // пушим полученные в параметре игредиенты в проперти-массив ingredients
    this.ingredients.push(ingredient);
    // используем eventemitter
    this.ingredientChanged.next(this.ingredients.slice());
  }

  //добавляем метод для добавление ингредиентов из recipe в shoppinglist
  addIngredients(ingredients: Ingredient[]) {
    // используем push и spread operator
    this.ingredients.push(...ingredients);
    // emitим ивент
    this.ingredientChanged.next(this.ingredients.slice());
  }

  // Добавим метод для отредактированного ингридиента, т.е. после того как мы кликнули на ингиридент изменили его amount или name не добавлять его в массив, а изменить его данные.
  // параметрами будет индекс ингридиента и новый игридиент
  updateIngredient(index: number, newIngredient: Ingredient) {
    // обращаемся к массиву ingredients с индексом ингридиента и устанавливаем ему значение newIngredient;
    this.ingredients[index] = newIngredient;
    // после вызываем ingredientChanged и эмитим обновление в массив и вызываем slice();
    this.ingredientChanged.next(this.ingredients.slice());
  }

  // добавим метод для удаления игридиента из массива игридиентов
  deteleIngredient(index: number) {
    //обращаемся к массиву игридиентов и вызываем метод splice, вторым аргументом дабавляем кол-во элементов, которое должно быть удалено из массива - 1.
    this.ingredients.splice(index, 1);
    // после вызываем ingredientChanged и эмитим обновление в массив и вызываем slice();
    this.ingredientChanged.next(this.ingredients.slice());
  }
}
