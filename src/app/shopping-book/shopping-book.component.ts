import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from './shopping-list.service';


@Component({
  selector: 'app-shopping-book',
  templateUrl: './shopping-book.component.html',
  styleUrls: ['./shopping-book.component.css']
})
export class ShoppingBookComponent implements OnInit, OnDestroy {
  // добляем проперти ingredients который будет содержать нашу дату Ingredients
  ingredients: Ingredient[];
  // инжектируем ShoppingListService в компонент

  // Добавим хранилище для Subscription
  private subscription: Subscription

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    // инициилизируем наш проперти-массив с сервиса ShoppingListService который содержит ingredients
    this.ingredients = this.shoppingListService.getIngredients();
    // подписываемся на ingredientChanged ивент из сервиса ShoppingListService, когда ингредиент изменится - мы будем об этом знать и добавлять его в массив ингредиентов
    this.subscription = this.shoppingListService.ingredientChanged.subscribe(
      (ingredients: Ingredient[]) => {
        this.ingredients = ingredients;
      }
    );
  }

  onEditItem(index: number) {
    this.shoppingListService.startedEditing.next(index);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
