import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit, OnDestroy {
  // получаем доступ к форме через ViewChild для того чтобы засетить значения полей в редактировании ингридиента
  @ViewChild('form', { static: false }) shoppingListForm: NgForm;
  // создаю property subscription с типом Subscription
  subscription: Subscription;
  // создаю проперти для храмения значения editMode
  editMode = false;
  // создаю проперти где буду хранить данные об полученном индексе
  editedItemIndex: number;
  // тут храним данные о editted item
  editedItem: Ingredient;
  // инжектируем ShoppingListService в компонент
  constructor(private shoppingListService: ShoppingListService) {}

  ngOnInit() {
    // обзервбл который дает нам возможность редактировать ингридиент по клику в форме shopping list edit;
    this.subscription = this.shoppingListService.startedEditing.subscribe(
      (index: number) => {
        this.editedItemIndex = index;
        this.editMode = true;
        // инорфмация о редактируемом игридиенте полученная из service
        this.editedItem = this.shoppingListService.getIngredient(index);
        this.shoppingListForm.setValue({
          name: this.editedItem.name,
          amount: this.editedItem.amount
        });
      }
    );
  }
  // Для добавления ингредиентов передадим в метод параметр form с типом ngForm тем самым получим доступ к форме
  onSubmit(form: NgForm) {
    // получим доступ к полям формы и их значениям,создадим переменную value где будем хранить значение form.value, т.е. значение полей формы, в последующем мы сможем обращаться к этим значениям через value.name, value.amount
    const value = form.value;
    // переменная для хранения данных ингридиентов полей name и amount по индексу
    const newIngredient = new Ingredient(value.name, value.amount);
    // вызываем метод updatedIngrediets если мы в edit mode для этого используем конструкию if ()
    // в которой мы проверяем, если editMode = true тогда ингридент updated в противном случае добавлятся новый
    if (this.editMode) {
      this.shoppingListService.updateIngredient(
        this.editedItemIndex,
        newIngredient
      );
    } else {
      // вызываем метод из сервиса ShoppingListService для добавления индгредиента в массив
      this.shoppingListService.addIngredient(newIngredient);
    }
    // После сабминта формы, выходим из editMode для возможности добавить новый ингридиент, или обновить существующий корректно
    this.editMode = false;
    // после сабмита формы, чистим поля формы
    form.reset();
  }
  // метод ресета полей формы
  onClearForm() {
    this.shoppingListForm.reset();
    this.editMode = false;
  }
  // метод удаления игридиентов
  // нужно сообщить сервису, что мы хотим удалить игридиента из массива игридиентов;
  onDelete() {
    // вызываем метод из сервиса в который передаем индекс редактируемого игридиента;
    this.shoppingListService.deteleIngredient(this.editedItemIndex);
    //вызываем reset формы;
    this.onClearForm();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
