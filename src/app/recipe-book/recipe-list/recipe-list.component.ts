import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Recipe } from '../recipe-book.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {
  // по умолчанию recipes будет undefined
  recipes: Recipe[];

  subscription: Subscription;

  // для получения доступа к сервису нужно инжектировать его в контструктор
  // инжектируем Router в constructor для использования данных в метод для добавления нового рецепта
  // инжектируем route для инормирования Router о нашем CurrentRoute
  constructor(
    private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.subscription = this.recipeService.recipesChanged.subscribe(
      (recipes: Recipe[]) => {
        this.recipes = recipes;
      }
    );
    //используем метод getRecipes(), который вернет нам копию массива рецептов, в ngOnInit, для рендра рецептов странице из сервиса RecipeService.
    this.recipes = this.recipeService.getRecipes();
  }
  // Метод для добавления нового рецепта при click
  onNewRecipe() {
    // Для навигации используем инжектируемый router и метод navigate
    this.router.navigate(['new'], { relativeTo: this.route });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
