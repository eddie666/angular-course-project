import { Injectable } from '@angular/core';

import { Recipe } from './recipe-book.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-book/shopping-list.service';
import { Subject } from 'rxjs';

// создаем сервис RecipeService

// чтобы получить доступ из сервиса к сервису нужно добавить декоратор @Injectable
@Injectable()
export class RecipeService {

  // добавлю проперти для обновления массива recipes когда добавлен новый рецент или обновлен существующий
  recipesChanged = new Subject<Recipe[]>();

  // добавляем в сервис массив рецептов
  private recipes: Recipe[] = [
    new Recipe(
      'Recipe of Salad',
      'This is a recipe of Salad',
      'https://www.wellplated.com/wp-content/uploads/2017/12/Hoppin-John-recipe-600x629.jpg',
      [new Ingredient('Beans', 20), new Ingredient('Salad', 5)]
    ),
    new Recipe(
      'Another Recipe of Some Meal',
      'This is a recipe of Some Meal',
      'https://www.wellplated.com/wp-content/uploads/2017/12/Hoppin-John-recipe-600x629.jpg',
      [new Ingredient('Meat', 2), new Ingredient('Rise', 20)]
    )
  ];

  // инжектируем shoppinglistservice в recipeservice
  constructor(private shoppingListService: ShoppingListService) {}

  // добавляем метод getRecipes() для первичного рендера наших рецептов на странице, с использованием метода slice(), который вернет копию массива
  getRecipes() {
    return this.recipes.slice();
  }

  // Добавим метод в сервис для передачи Id в route и в дальнейшем это позволит грузить single-recipe по id
  getRecipe(index: number) {
    return this.recipes[index];
  }

  // добавим метод в сервисе который будет добавлять репецп в shoppinglist
  addIngToShoppingList(ingredients: Ingredient[]) {
    // тут нам нужен доступ в shoppinglistservice для этого добавляем декоратор @Injectable и инжектируем сервис в конструктор
    this.shoppingListService.addIngredients(ingredients);
  }
  // метод для добавления нового рецепта, ожидаемые параметры recipe с типом Recipe
  addNewRecipe(recipe: Recipe) {
    // берем массив рецептов recipe и пушим в него полученный новый объект recipe с типом Recipe
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }
  // метод апдейта рецепта ожидаемые параметры index c типом number и новое значение newRecipe с типом Recipe;
  updateRecipe(index: number, newRecipe: Recipe) {
    // обращаемся к массиву recipe и индексу полученному в параметре и присваеваем полученное значение в newRecipe;
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }
  deleteRecipe(index: number) {
    this.recipes.splice(index, 1)
    this.recipesChanged.next(this.recipes.slice());
  }
}
