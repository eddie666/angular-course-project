import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Recipe } from '../recipe-book.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipe: Recipe;
  // 1. Для получения ID из Route: добавим проперти id куда будем присваивать полученный из роутинга Id
  id: number;

  // инжектируем RecipeService
  // 1.1 инжектируем ActivatedRoute для получения ID рецепта
  constructor(
    private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    // 2. получаем ID из Route есть 2 разных метода получить Id
    // 1:
    // const id = this.route.snapshot.params['id'];
    // 2: используем обзервбл и подписываемся на событие о изменении параметров:
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      // Fetch рецепта из сервиса RecipeService с помощью метода getRecipes с аргументом this.id т.е. нашим проперти кому присваемваем полученный Id из сервиса
      this.recipe = this.recipeService.getRecipe(this.id);
    });
  }
  // добавим метод, который будет посылать наш рецеп в shoppingList
  addToShoppingList() {
    this.recipeService.addIngToShoppingList(this.recipe.ingredients);
  }
  // Метод для Edit при click
  onEditRecipe() {
    // используем router для и метод navigate() в нем используем инжектируемый acitvateroute дабы понять свой CurrentRoute и использовать его в relativeTo
    this.router.navigate(['edit'], { relativeTo: this.route });
    // this.router.navigate(['../', this.id ,'edit'], { relativeTo: this.route });
  }
  onDeleteRecipe(index: number) {
    this.recipeService.deleteRecipe(index);
    this.router.navigate(['/recipes'], {relativeTo: this.route})
  }
}
