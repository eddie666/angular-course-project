import { Directive, HostListener, HostBinding, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  // добавляем директиву isOpen которая по умолчанию будет значением false, и используем декоратор @HostBinding с классом class.open
  @HostBinding('class.open') isOpen = false;

  // для того чтобы слушать клик вне компонента нужно использовать директиву @HostListener
  // клик на document для закрытия dropdown
  @HostListener('document:click', ['$event']) toggleOpen(event: Event) {
    // при клике меняем значение isOpen с false на противоположное значение true с помощью !this.isOpen
    this.isOpen = this.elRef.nativeElement.contains(event.target) ? !this.isOpen : false;
  }
  // добавляем elementRef
  constructor(private elRef: ElementRef) {}
}
